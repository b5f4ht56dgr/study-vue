//console.log('ok');

import Vue from 'vue';
Vue.config.devtools = true; //开启调试模式

//导入路由包
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import router from './router.js';

import moment from 'moment';
//定义全局过滤器
Vue.filter('dateFormat', function (dataStr, pattern = "YYYY-MM-DD HH:mm:ss") {
  return moment(dataStr).format(pattern);
});

//导入vue-resource
import VueResource from 'vue-resource';
Vue.use(VueResource);

//导入MUI的样式
import './lib/mui/css/mui.min.css';
import './lib/mui/css/icons-extra.css';

//导入Mint-UI组件
import MintUI from 'mint-ui';
Vue.use(MintUI);
import 'mint-ui/lib/style.css';

//图片预览组件
import VuePreview from 'vue-preview';
Vue.use(VuePreview, {
  mainClass: 'pswp--minimal--dark',
  barsSize: { top: 0, bottom: 0 },
  captionEl: false,
  fullscreenEl: false,
  shareEl: false,
  bgOpacity: 0.85,
  tapToClose: true,
  tapToToggleControls: false
})

import app from './App.vue';

var vm = new Vue({
  el: '#app',
  render: c => c(app),
  router
});